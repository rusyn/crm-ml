﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Newtonsoft.Json.Linq;

namespace Sandbox.AzureMachineLearning
{
    internal class ApiResponseMetadata<T>
    {
        private readonly List<ApiPropertyMetadataConfig<T>> configs;

        public ApiResponseMetadata()
        {
            this.configs = new List<ApiPropertyMetadataConfig<T>>();
        }

        public void Add(Expression<Func<T, object>> property, Action<PropertyBasedApiMetadataPropertyConfig<T>> setConfig = null)
        {
            PropertyBasedApiMetadataPropertyConfig<T> config = new PropertyBasedApiMetadataPropertyConfig<T>(property);

            if (setConfig != null)
            {
                setConfig(config);
            }

            this.configs.Add(config);
        }

        public void Add(string propertyName, Action<StaticApiRequestMetadataPropertyConfig<T>> setConfig)
        {
            StaticApiRequestMetadataPropertyConfig<T> config = new StaticApiRequestMetadataPropertyConfig<T>(propertyName);

            if (setConfig != null)
            {
                setConfig(config);
            }

            this.configs.Add(config);
        }
    }
}