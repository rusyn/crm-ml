﻿namespace Sandbox.AzureMachineLearning
{
    internal class StaticApiRequestMetadataPropertyConfig<T> : ApiPropertyMetadataConfig<T>
    {
        private readonly string propertyName;

        public StaticApiRequestMetadataPropertyConfig(string propertyName)
        {
            this.propertyName = propertyName;
        }

        public override string PropertyName
        {
            get
            {
                return this.propertyName;
            }
        }

        public bool Nullable
        {
            get;
            set;
        }

        public string StaticValue
        {
            get;
            set;
        }

        public override string GetValue(T instance)
        {
            return this.StaticValue;
        }

        public override void SetValue(T instance, object rawValue)
        {
            throw new System.NotSupportedException();
        }
    }
}