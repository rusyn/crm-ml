﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Newtonsoft.Json.Linq;

namespace Sandbox.AzureMachineLearning
{
    internal class ApiRequestMetadata<T>
    {
        private readonly List<ApiPropertyMetadataConfig<T>> configs;

        public ApiRequestMetadata()
        {
            this.configs = new List<ApiPropertyMetadataConfig<T>>();
        }

        public void Add(Expression<Func<T, object>> property, Action<PropertyBasedApiMetadataPropertyConfig<T>> setConfig = null)
        {
            PropertyBasedApiMetadataPropertyConfig<T> config = new PropertyBasedApiMetadataPropertyConfig<T>(property);

            if (setConfig != null)
            {
                setConfig(config);
            }

            this.configs.Add(config);
        }

        public void Add(string propertyName, Action<StaticApiRequestMetadataPropertyConfig<T>> setConfig)
        {
            StaticApiRequestMetadataPropertyConfig<T> config = new StaticApiRequestMetadataPropertyConfig<T>(propertyName);

            if (setConfig != null)
            {
                setConfig(config);
            }

            this.configs.Add(config);
        }

        public void Serialise(T data, JObject input)
        {
            JArray columnNames = new JArray();
            input.Add("ColumnNames", columnNames);

            JArray valuesContainer = new JArray();
            input.Add("Values", valuesContainer);

            JArray instanceValues = new JArray();
            valuesContainer.Add(instanceValues);

            foreach (ApiPropertyMetadataConfig<T> config in this.configs)
            {
                columnNames.Add(config.PropertyName);

                string value = config.GetValue(data);
                instanceValues.Add(value);
            }
        }

        public void Deserialise(T instance, JObject response)
        {
            JToken results = response["Results"];
            JToken outputContainer = results["output1"];
            JObject valueContainer = (JObject)outputContainer["value"];

            Dictionary<string, string> columnValueLookup = this.CreateColumnValueLookup(valueContainer);

            foreach (ApiPropertyMetadataConfig<T> propertyMetadata in this.configs)
            {
                string propertyValue = columnValueLookup[propertyMetadata.PropertyName];

                propertyMetadata.SetValue(instance, propertyValue);
            }
        }

        private Dictionary<string, string> CreateColumnValueLookup(JObject valueContainer)
        {
            Dictionary<string, string> lookup = new Dictionary<string, string>();

            JArray columnNames = (JArray)valueContainer["ColumnNames"];
            JArray values = (JArray)valueContainer["Values"][0];

            for (int i = 0; i < columnNames.Count; i++)
            {
                lookup.Add(columnNames[i].Value<string>(), values[i].Value<string>());
            }

            return lookup;
        }
    }
}