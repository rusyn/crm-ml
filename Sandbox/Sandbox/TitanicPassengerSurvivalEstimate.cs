﻿namespace Sandbox
{
    internal class TitanicPassengerSurvivalEstimate
    {
        public decimal SurvivalProbability
        {
            get;
            set;
        }
    }
}