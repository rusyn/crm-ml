﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Sandbox.AzureMachineLearning;

namespace Sandbox
{
    internal class TitanicSurvivabilityRequest
    {
        private static readonly ApiRequestMetadata<TitanicPassenger> Metadata;

        static TitanicSurvivabilityRequest()
        {
            TitanicSurvivabilityRequest.Metadata = new ApiRequestMetadata<TitanicPassenger>();

            TitanicSurvivabilityRequest.Metadata.Add(passenger => passenger.PassengerId);
            TitanicSurvivabilityRequest.Metadata.Add("Survived",
                                                     config =>
                                                     {
                                                         config.StaticValue = "0";
                                                     });

            TitanicSurvivabilityRequest.Metadata.Add(passenger => passenger.PassengerClass,
                                                     config =>
                                                     {
                                                         config.ApiPropertyName = "Pclass";
                                                     });

            TitanicSurvivabilityRequest.Metadata.Add(passenger => passenger.PassengerName,
                                                     config =>
                                                     {
                                                         config.ApiPropertyName = "Name";
                                                     });

            TitanicSurvivabilityRequest.Metadata.Add(passenger => passenger.Sex);
            TitanicSurvivabilityRequest.Metadata.Add(passenger => passenger.Age);
            TitanicSurvivabilityRequest.Metadata.Add(passenger => passenger.SiblingOrSpouseCount, config => config.ApiPropertyName = "SibSp");
            TitanicSurvivabilityRequest.Metadata.Add(passenger => passenger.ParentsOrChildrenCount, config => config.ApiPropertyName = "Parch");
            TitanicSurvivabilityRequest.Metadata.Add(passenger => passenger.Ticket);
            TitanicSurvivabilityRequest.Metadata.Add(passenger => passenger.Fare);
            TitanicSurvivabilityRequest.Metadata.Add(passenger => passenger.Cabin);
            TitanicSurvivabilityRequest.Metadata.Add(passenger => passenger.Embarked);
        }

        public TitanicPassenger Passenger
        {
            get;
            set;
        }

        public async Task<TitanicSurvivabilityResponse> Execute()
        {
            JObject requestContent = this.Serialise();

            using (var client = new HttpClient())
            {
                const string apiKey = "emL3uaPVc+SBF/Iv6QudhocomW4CNFfWWvt/3oFZv+se2pxdzYwN9HH736mqDLMNgWcSfpRV2PTO2VdW9AOx9g==";

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", apiKey);

                client.BaseAddress =
                    new Uri(
                        "https://ussouthcentral.services.azureml.net/workspaces/eaaabf3396044cbbacbec6a16312fd5e/services/28a7311a849e479f93bf028b4dc9223c/execute?api-version=2.0&details=true");

                HttpResponseMessage response = await client.PostAsJsonAsync("", requestContent);

                TitanicSurvivabilityResponse survivabilityResponse = new TitanicSurvivabilityResponse();
                survivabilityResponse.WasSuccessful = response.IsSuccessStatusCode;
                survivabilityResponse.ResponseCode = response.StatusCode;
                survivabilityResponse.Headers = response.Headers;
                survivabilityResponse.RawResponse = await response.Content.ReadAsStringAsync();

                if (response.IsSuccessStatusCode)
                {
                    survivabilityResponse.Parse(JObject.Parse(survivabilityResponse.RawResponse));
                }

                return survivabilityResponse;
            }
        }

        private JObject Serialise()
        {
            JObject requestRoot = new JObject();

            JObject inputs = new JObject();
            requestRoot.Add("Inputs", inputs);

            JObject passengerInput = new JObject();
            inputs.Add("input1", passengerInput);
            TitanicSurvivabilityRequest.Metadata.Serialise(this.Passenger, passengerInput);

            JObject globalParameters = new JObject();
            requestRoot.Add("GlobalParameters", globalParameters);

            return requestRoot;
        }
    }
}