﻿using System;

namespace Sandbox
{
    public class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Program.DoSomething();
            }
            catch (Exception e)
            {
                Console.WriteLine("***************");
                Console.WriteLine(e);
            }

            Console.WriteLine("\n\nDone.");
            Console.ReadKey(true);
        }

        private static void DoSomething()
        {
            TitanicPassenger passenger = new TitanicPassenger();
            passenger.PassengerId = 892;
            passenger.PassengerClass = 3;
            passenger.PassengerName = "Kelly, Mr. James";
            passenger.Sex = "male";
            passenger.Age = 34;
            passenger.SiblingOrSpouseCount = 0;
            passenger.ParentsOrChildrenCount = 0;
            passenger.Ticket = "330911";
            passenger.Fare = 7.8292;
            passenger.Cabin = null;
            passenger.Embarked = "Q";

            TitanicSurvivabilityRequest request = new TitanicSurvivabilityRequest();
            request.Passenger = passenger;

            // TODO: Get response.
            TitanicSurvivabilityResponse survivabilityResponse = request.Execute().Result;

            if (survivabilityResponse.WasSuccessful)
            {
                Console.WriteLine("Success: {0}", survivabilityResponse.Response.SurvivalProbability);
            }
            else
            {
                Console.WriteLine("Unsuccessful, response was: ");
                Console.WriteLine(survivabilityResponse.Headers);
                Console.WriteLine(survivabilityResponse.RawResponse);
            }
        }
    }
}