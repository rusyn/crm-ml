﻿using System.Net;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;
using Sandbox.AzureMachineLearning;

namespace Sandbox
{
    internal class TitanicSurvivabilityResponse
    {
        private static readonly ApiRequestMetadata<TitanicPassengerSurvivalEstimate> Metadata;

        static TitanicSurvivabilityResponse()
        {
            TitanicSurvivabilityResponse.Metadata = new ApiRequestMetadata<TitanicPassengerSurvivalEstimate>();

            TitanicSurvivabilityResponse.Metadata.Add(passenger => passenger.SurvivalProbability,
                                                      config =>
                                                      {
                                                          config.ApiPropertyName = "SurvivalProbability";
                                                      });
        }

        public bool WasSuccessful
        {
            get;
            set;
        }

        public HttpStatusCode ResponseCode
        {
            get;
            set;
        }

        public HttpResponseHeaders Headers
        {
            get;
            set;
        }

        public string RawResponse
        {
            get;
            set;
        }

        public TitanicPassengerSurvivalEstimate Response
        {
            get;
            set;
        }

        public void Parse(JObject responseContent)
        {
            TitanicPassengerSurvivalEstimate survivalEstimate = new TitanicPassengerSurvivalEstimate();
            TitanicSurvivabilityResponse.Metadata.Deserialise(survivalEstimate, responseContent);

            this.Response = survivalEstimate;
        }
    }
}