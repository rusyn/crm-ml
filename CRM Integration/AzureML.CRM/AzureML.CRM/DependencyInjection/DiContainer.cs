﻿using System;
using System.Activities;
using Autofac;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;

namespace AzureML.CRM.DependencyInjection
{
    internal class DiContainer : IDiContainer, IDisposable
    {
        private readonly IContainer container;

        public DiContainer(CodeActivityContext context)
        {
            ContainerBuilder containerBuilder = new ContainerBuilder();

            containerBuilder.RegisterInstance(context);

            containerBuilder.RegisterCrmExtension<ITracingService>(context);
            containerBuilder.RegisterCrmExtension<IWorkflowContext>(context);
            containerBuilder.RegisterCrmExtension<IOrganizationServiceFactory>(context);

            containerBuilder.Register(
                componentContext =>
                {
                    IOrganizationServiceFactory organizationServiceFactory = componentContext.Resolve<IOrganizationServiceFactory>();
                    IWorkflowContext workflowContext = componentContext.Resolve<IWorkflowContext>();

                    IOrganizationService organizationService = organizationServiceFactory.CreateOrganizationService(workflowContext.UserId);

                    return organizationService;
                }).InstancePerLifetimeScope();

            this.RegiserWorkflows(containerBuilder);

            this.container = containerBuilder.Build();
        }

        private void RegiserWorkflows(ContainerBuilder containerBuilder)
        {
            containerBuilder.RegisterType<TitanicPopulateSurvivabilityWorkflow>().AsSelf();
        }

        public T Create<T>()
        {
            return this.container.Resolve<T>();
        }

        public void Dispose()
        {
            this.container.Dispose();
        }
    }
}