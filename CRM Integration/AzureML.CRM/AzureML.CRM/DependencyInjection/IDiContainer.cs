﻿namespace AzureML.CRM.DependencyInjection
{
    internal interface IDiContainer
    {
        T Create<T>();
    }
}