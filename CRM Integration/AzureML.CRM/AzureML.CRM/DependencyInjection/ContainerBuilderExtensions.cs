﻿using System.Activities;
using Autofac;

namespace AzureML.CRM.DependencyInjection
{
    internal static class ContainerBuilderExtensions
    {
        public static void RegisterCrmExtension<T>(this ContainerBuilder containerBuilder, ActivityContext context)
            where T : class
        {
            containerBuilder.Register(componentContext => context.GetExtension<T>())
                            .ExternallyOwned();
        }
    }
}