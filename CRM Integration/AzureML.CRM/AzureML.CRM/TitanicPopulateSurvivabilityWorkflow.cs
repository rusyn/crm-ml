﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AzureML.CRM.CrmCore;
using AzureML.CRM.CrmExtensions;
using AzureML.CRM.TitanicML;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Workflow;

namespace AzureML.CRM
{
    internal class TitanicPopulateSurvivabilityWorkflow : IWorkflow<TitanicPopulateSurvivabilityArguments>
    {
        private readonly IOrganizationService organizationService;
        private readonly IWorkflowContext workflowContext;
        private readonly ITracingService traceService;

        public TitanicPopulateSurvivabilityWorkflow(
            IOrganizationService organizationService,
            IWorkflowContext workflowContext,
            ITracingService traceService)
        {
            this.organizationService = organizationService;
            this.workflowContext = workflowContext;
            this.traceService = traceService;
        }

        public void Execute(TitanicPopulateSurvivabilityArguments arguments)
        {
            // Assumes lead...
            this.traceService.Trace("Starting TitanicPopulateSurvivabilityWorkflow, {0}", DateTime.Now);
            this.traceService.Trace("Culture: {0}", Thread.CurrentThread.CurrentCulture.Name);
            this.traceService.Trace("UI culture: {0}", Thread.CurrentThread.CurrentUICulture.Name);

            EntityReference leadReference = new EntityReference(this.workflowContext.PrimaryEntityName, this.workflowContext.PrimaryEntityId);
            Lead lead = new Lead(leadReference, this.organizationService, this.traceService);

            TitanicSurvivabilityResponse survivabilityEstimate = this.RequestSurvivabilityEstimate(lead);

            this.UpdateEntityWithEstimate(survivabilityEstimate, lead);
        }

        private void UpdateEntityWithEstimate(
            TitanicSurvivabilityResponse survivabilityEstimate,
            Lead lead)
        {
            lead.SurvivalPotential = survivabilityEstimate.Response.SurvivalProbability;

            lead.Update();
        }

        private TitanicSurvivabilityResponse RequestSurvivabilityEstimate(Lead lead)
        {
            // TODO: Wrap request in service.
            TitanicSurvivabilityRequest survivabilityRequest = this.CreateSurvivabilityRequest(lead);

            Task<TitanicSurvivabilityResponse> requestTask = survivabilityRequest.Execute(this.traceService);

            // TODO: Timeout error handling.
            requestTask.Wait(TimeSpan.FromSeconds(30));

            return requestTask.Result;
        }

        private TitanicSurvivabilityRequest CreateSurvivabilityRequest(Lead lead)
        {
            TitanicPassenger passenger = new TitanicPassenger();

            passenger.Cabin = lead.Cabin;
            passenger.ParentsOrChildrenCount = lead.ParentsOrChildrenCount;
            passenger.Age = lead.Age;
            passenger.Fare = (double) lead.Fare;
            passenger.PassengerClass = lead.PassengerClass;
            passenger.Sex = lead.Sex;
            passenger.SiblingOrSpouseCount = lead.SiblingOrSpouseCount;

            TitanicSurvivabilityRequest survivabilityRequest = new TitanicSurvivabilityRequest();
            survivabilityRequest.Passenger = passenger;

            return survivabilityRequest;
        }
    }
}