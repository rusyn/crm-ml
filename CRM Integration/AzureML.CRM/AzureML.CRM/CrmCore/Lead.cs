﻿using System.Collections.Generic;
using System.Text;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;

namespace AzureML.CRM.CrmCore
{
    internal class Lead : CrmEntity
    {
        private Schema schema;

        public Lead(
            EntityReference entityReference,
            IOrganizationService organizationService,
            ITracingService traceService)
            : base(entityReference, organizationService, traceService)
        {
        }

        private Schema EntitySchema
        {
            get
            {
                return this.schema ?? (this.schema = new Schema(this.TraceService));
            }
        }

        public decimal SurvivalPotential
        {
            get
            {
                return this.EntitySchema.SurvivalPotential.GetValue();
            }
            set
            {
                this.EntitySchema.SurvivalPotential.SetValue(value);
            }
        }

        public int Age
        {
            get
            {
                return this.EntitySchema.Age.GetValue();
            }
            set
            {
                this.EntitySchema.Age.SetValue(value);
            }
        }

        public string Cabin
        {
            get
            {
                return this.EntitySchema.Cabin.GetValue();
            }
            set
            {
                this.EntitySchema.Cabin.SetValue(value);
            }
        }

        public int ParentsOrChildrenCount
        {
            get
            {
                return this.EntitySchema.ParentsOrChildrenCount.GetValue();
            }
            set
            {
                this.EntitySchema.ParentsOrChildrenCount.SetValue(value);
            }
        }

        public int SiblingOrSpouseCount
        {
            get
            {
                return this.EntitySchema.SiblingOrSpouseCount.GetValue();
            }
            set
            {
                this.EntitySchema.SiblingOrSpouseCount.SetValue(value);
            }
        }

        public int PassengerClass
        {
            get
            {
                return this.EntitySchema.PassengerClass.GetValue();
            }
            set
            {
                this.EntitySchema.PassengerClass.SetValue(value);
            }
        }

        public string Sex
        {
            get
            {
                return this.EntitySchema.Sex.GetValue();
            }
            set
            {
                this.EntitySchema.Sex.SetValue(value);
            }
        }

        public decimal Fare
        {
            get
            {
                return this.EntitySchema.Fare.GetValue();
            }
            set
            {
                this.EntitySchema.Fare.SetValue(value);
            }
        }

        protected override void BindToEntity()
        {
            base.BindToEntity();

            this.EntitySchema.Bind(this.Entity);
        }

        public void Update()
        {
            this.TraceService.Trace("Updating entity...");
            this.TraceService.Trace("-- Retrieve entity...");

            Entity entityToUpdate = this.OrganizationService.Retrieve(this.Entity.LogicalName, this.Entity.Id, new ColumnSet(this.EntitySchema.SurvivalPotential.PropertyName));
            entityToUpdate[this.EntitySchema.SurvivalPotential.PropertyName] = this.SurvivalPotential;

            this.OrganizationService.Update(entityToUpdate);

            this.TraceService.Trace("-- Update complete.");
        }

        private void TraceAttributes()
        {
            StringBuilder log = new StringBuilder();

            log.AppendLine("---");
            log.AppendLine(this.EntityReference.LogicalName + "/" + this.EntityReference.Id);

            foreach (KeyValuePair<string, object> attribute in this.Entity.Attributes)
            {
                log.Append("  ").Append(attribute.Key).AppendLine();
            }

            log.AppendLine("---");

            this.TraceService.Trace(log.ToString());
        }

        private class Schema
        {
            public Schema(ITracingService traceService)
            {
                traceService.Trace("Bind: new_TitanicSurvivalPotential");
                this.SurvivalPotential = new FloatingPointProperty("new_TitanicSurvivalPotential", 1.00m);

                traceService.Trace("Bind: new_Age");
                this.Age = new IntegerProperty("new_Age");

                traceService.Trace("Bind: new_Fare");
                this.Fare = new FloatingPointProperty("new_Fare");

                traceService.Trace("Bind: new_Cabin");
                this.Cabin = new StringProperty("new_Cabin");

                traceService.Trace("Bind: new_ParentsOrChildrenCount");
                this.ParentsOrChildrenCount = new IntegerProperty("new_ParentsOrChildrenCount");

                traceService.Trace("Bind: new_SiblingOrSpouseCount");
                this.SiblingOrSpouseCount = new IntegerProperty("new_SiblingOrSpouseCount");

                traceService.Trace("Bind: new_PassengerClass");
                this.PassengerClass = new IntegerProperty("new_PassengerClass");

                traceService.Trace("Bind: new_Sex");
                this.Sex = new StringProperty("new_Sex");
            }

            public IntegerProperty ParentsOrChildrenCount
            {
                get;
                private set;
            }

            public IntegerProperty SiblingOrSpouseCount
            {
                get;
                private set;
            }

            public IntegerProperty PassengerClass
            {
                get;
                private set;
            }

            public StringProperty Cabin
            {
                get;
                private set;
            }

            public FloatingPointProperty Fare
            {
                get;
                private set;
            }

            public StringProperty Sex
            {
                get;
                private set;
            }

            public FloatingPointProperty SurvivalPotential
            {
                get;
                private set;
            }

            public IntegerProperty Age
            {
                get;
                private set;
            }

            public void Bind(Entity entity)
            {
                this.SurvivalPotential.Bind(entity);
                this.Age.Bind(entity);
                this.ParentsOrChildrenCount.Bind(entity);
                this.PassengerClass.Bind(entity);
                this.Cabin.Bind(entity);
                this.Sex.Bind(entity);
                this.SiblingOrSpouseCount.Bind(entity);
                this.Fare.Bind(entity);
            }
        }
    }
}