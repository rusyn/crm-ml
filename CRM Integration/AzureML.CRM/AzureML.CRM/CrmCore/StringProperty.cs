﻿using System;

namespace AzureML.CRM.CrmCore
{
    internal class StringProperty : EntityProperty<string>
    {
        public StringProperty(string propertyName) : base(propertyName)
        {
        }

        public override string GetValue()
        {
            return Convert.ToString(this.GetPropertyValue());
        }

        public override void SetValue(string value)
        {
            this.SetPropertyValue(value);
        }
    }
}