﻿using System;

namespace AzureML.CRM.CrmCore
{
    internal class DateTimeProperty : EntityProperty<DateTime>
    {
        public DateTimeProperty(string propertyName) : base(propertyName)
        {
        }

        public override DateTime GetValue()
        {
            return Convert.ToDateTime(this.GetPropertyValue());
        }

        public override void SetValue(DateTime value)
        {
            this.SetPropertyValue(value);
        }
    }
}