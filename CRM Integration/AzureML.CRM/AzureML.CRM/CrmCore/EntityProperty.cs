﻿using Microsoft.Xrm.Sdk;

namespace AzureML.CRM.CrmCore
{
    internal abstract class EntityProperty<T>
    {
        protected EntityProperty(string propertyName)
        {
            this.PropertyName = propertyName.ToLower();
        }

        public string PropertyName
        {
            get;
            private set;
        }

        private Entity Entity
        {
            get;
            set;
        }

        public virtual void Bind(Entity entity)
        {
            this.Entity = entity;
        }

        public abstract T GetValue();

        public abstract void SetValue(T value);

        protected virtual object GetPropertyValue()
        {
            this.AssertPropertyIsBound();

            object value = this.Entity[this.PropertyName];

            return value;
        }

        protected virtual void SetPropertyValue(object value)
        {
            this.AssertPropertyIsBound();

            this.Entity.Attributes[this.PropertyName] = value;
        }

        private void AssertPropertyIsBound()
        {
            if (this.Entity == null)
            {
                throw new EntityNotBoundException(string.Format("Entity has not been bound, {0} property on field '{1}'",
                                                                this.GetType().FullName,
                                                                this.PropertyName));
            }
        }
    }
}