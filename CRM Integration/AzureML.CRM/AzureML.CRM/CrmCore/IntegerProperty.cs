using System;

namespace AzureML.CRM.CrmCore
{
    internal class IntegerProperty : EntityProperty<int>
    {
        public IntegerProperty(string propertyName) : base(propertyName)
        {
        }

        public override int GetValue()
        {
            return Convert.ToInt32(this.GetPropertyValue());
        }

        public override void SetValue(int value)
        {
            this.SetPropertyValue(value);
        }
    }
}