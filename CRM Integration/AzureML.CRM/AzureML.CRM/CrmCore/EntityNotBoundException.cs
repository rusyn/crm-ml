﻿using System;

namespace AzureML.CRM.CrmCore
{
    public class EntityNotBoundException : Exception
    {
        public EntityNotBoundException(string message) : base(message)
        {
        }
    }
}