﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Query;

namespace AzureML.CRM.CrmCore
{
    internal abstract class CrmEntity
    {
        protected CrmEntity(EntityReference entityReference, IOrganizationService organizationService, ITracingService traceService)
        {
            this.TraceService = traceService;
            this.EntityReference = entityReference;
            this.OrganizationService = organizationService;

            this.InternalLoadEntity();
        }

        public ITracingService TraceService
        {
            get;
            private set;
        }

        public EntityReference EntityReference
        {
            get;
            private set;
        }

        public Entity Entity
        {
            get;
            private set;
        }

        protected IOrganizationService OrganizationService
        {
            get;
            private set;
        }

        public object this[string propertyName]
        {
            get
            {
                return this.Entity[propertyName];
            }
        }

        private void InternalLoadEntity()
        {
            string entityTypeName = this.GetType().Name;

            this.TraceService.Trace("Loading {0} entity for reference {1}/{2}", entityTypeName, this.EntityReference.LogicalName, this.EntityReference.Id);
            RetrieveRequest retrieveContactRequest = new RetrieveRequest();

            // TODO: Delegate column selection to inheritor.
            retrieveContactRequest.ColumnSet = new ColumnSet(true);
            retrieveContactRequest.Target = this.EntityReference;

            RetrieveResponse response = (RetrieveResponse) this.OrganizationService.Execute(retrieveContactRequest);
            this.TraceService.Trace("Load {0} entity response: have entity = {1}", entityTypeName, response.Entity != null);

            this.Entity = response.Entity;

            this.TraceService.Trace("Binding to {0} entity.", entityTypeName);
            this.BindToEntity();
        }

        protected virtual void BindToEntity()
        {
        }
    }
}