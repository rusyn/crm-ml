﻿using System;

namespace AzureML.CRM.CrmCore
{
    internal class FloatingPointProperty : EntityProperty<decimal>
    {
        private const decimal DefaultMaximumValue = 1000000;

        private readonly decimal maximumValue;

        public FloatingPointProperty(
            string propertyName,
            decimal maximumValue = FloatingPointProperty.DefaultMaximumValue) : base(propertyName)
        {
            this.maximumValue = maximumValue;
        }

        public override decimal GetValue()
        {
            return Convert.ToDecimal(this.GetPropertyValue() ?? 0);
        }

        public override void SetValue(decimal value)
        {
            this.SetPropertyValue(value);
        }
    }
}