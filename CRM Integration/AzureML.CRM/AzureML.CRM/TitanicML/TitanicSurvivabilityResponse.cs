﻿using System.Net;
using System.Net.Http.Headers;
using AzureML.CRM.AzureMachineLearning;
using Microsoft.Xrm.Sdk;
using Newtonsoft.Json.Linq;

namespace AzureML.CRM.TitanicML
{
    internal class TitanicSurvivabilityResponse
    {
        private static readonly ApiRequestMetadata<TitanicPassengerSurvivalEstimate> Metadata;

        static TitanicSurvivabilityResponse()
        {
            TitanicSurvivabilityResponse.Metadata = new ApiRequestMetadata<TitanicPassengerSurvivalEstimate>();

            TitanicSurvivabilityResponse.Metadata.Add(passenger => passenger.SurvivalProbability,
                                                      config =>
                                                      {
                                                          config.ApiPropertyName = "SurvivalProbability";
                                                      });
        }

        public bool WasSuccessful
        {
            get;
            set;
        }

        public HttpStatusCode ResponseCode
        {
            get;
            set;
        }

        public HttpResponseHeaders Headers
        {
            get;
            set;
        }

        public string RawResponse
        {
            get;
            set;
        }

        public TitanicPassengerSurvivalEstimate Response
        {
            get;
            set;
        }

        public void Parse(JObject responseContent, ITracingService tracingService)
        {
            TitanicPassengerSurvivalEstimate survivalEstimate = new TitanicPassengerSurvivalEstimate();
            TitanicSurvivabilityResponse.Metadata.Deserialise(survivalEstimate, responseContent, tracingService);

            this.Response = survivalEstimate;
        }
    }
}