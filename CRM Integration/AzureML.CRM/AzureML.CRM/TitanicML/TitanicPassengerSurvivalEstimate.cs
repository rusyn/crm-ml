﻿namespace AzureML.CRM.TitanicML
{
    internal class TitanicPassengerSurvivalEstimate
    {
        public decimal SurvivalProbability
        {
            get;
            set;
        }
    }
}