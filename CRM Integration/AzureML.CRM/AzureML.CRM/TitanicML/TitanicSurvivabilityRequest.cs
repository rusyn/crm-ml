﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using AzureML.CRM.AzureMachineLearning;
using Microsoft.Xrm.Sdk;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AzureML.CRM.TitanicML
{
    internal class TitanicSurvivabilityRequest
    {
        private static readonly ApiRequestMetadata<TitanicPassenger> Metadata;

        static TitanicSurvivabilityRequest()
        {
            TitanicSurvivabilityRequest.Metadata = new ApiRequestMetadata<TitanicPassenger>();

            TitanicSurvivabilityRequest.Metadata.Add(passenger => passenger.PassengerId);
            TitanicSurvivabilityRequest.Metadata.Add("Survived",
                                                     config =>
                                                     {
                                                         config.StaticValue = "0";
                                                     });

            TitanicSurvivabilityRequest.Metadata.Add(passenger => passenger.PassengerClass,
                                                     config =>
                                                     {
                                                         config.ApiPropertyName = "Pclass";
                                                     });

            TitanicSurvivabilityRequest.Metadata.Add(passenger => passenger.PassengerName,
                                                     config =>
                                                     {
                                                         config.ApiPropertyName = "Name";
                                                     });

            TitanicSurvivabilityRequest.Metadata.Add(passenger => passenger.Sex);
            TitanicSurvivabilityRequest.Metadata.Add(passenger => passenger.Age);
            TitanicSurvivabilityRequest.Metadata.Add(passenger => passenger.SiblingOrSpouseCount, config => config.ApiPropertyName = "SibSp");
            TitanicSurvivabilityRequest.Metadata.Add(passenger => passenger.ParentsOrChildrenCount, config => config.ApiPropertyName = "Parch");
            TitanicSurvivabilityRequest.Metadata.Add(passenger => passenger.Ticket);
            TitanicSurvivabilityRequest.Metadata.Add(passenger => passenger.Fare);
            TitanicSurvivabilityRequest.Metadata.Add(passenger => passenger.Cabin);
            TitanicSurvivabilityRequest.Metadata.Add(passenger => passenger.Embarked);
        }

        public TitanicPassenger Passenger
        {
            get;
            set;
        }

        public async Task<TitanicSurvivabilityResponse> Execute(ITracingService tracingService)
        {
            JObject requestContent = this.Serialise();

            tracingService.Trace("Submitting to Azure:\n" + requestContent.ToString(Formatting.Indented));

            using (var client = new HttpClient())
            {
                const string apiKey = "5Vazg8Xtwd5uoVpiLtnIulVG8ZDcM0bQXmlaouE4d84c4SH9P1qlf0o2gWE+Xhlw4nerARqRpcESdL5juTlegg==";

                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", apiKey);

                client.BaseAddress =
                    new Uri(
                        "https://ussouthcentral.services.azureml.net/workspaces/eaaabf3396044cbbacbec6a16312fd5e/services/e161e6dca3b74993873ce68b0cabf0aa/execute?api-version=2.0&details=true");

                HttpResponseMessage response = await client.PostAsJsonAsync("", requestContent);

                TitanicSurvivabilityResponse survivabilityResponse = new TitanicSurvivabilityResponse();
                survivabilityResponse.WasSuccessful = response.IsSuccessStatusCode;
                survivabilityResponse.ResponseCode = response.StatusCode;
                survivabilityResponse.Headers = response.Headers;
                survivabilityResponse.RawResponse = await response.Content.ReadAsStringAsync();

                tracingService.Trace("Azure response:\n" + survivabilityResponse.RawResponse);

                if (response.IsSuccessStatusCode)
                {
                    tracingService.Trace("Success code from AML, parsing.");
                    survivabilityResponse.Parse(JObject.Parse(survivabilityResponse.RawResponse), tracingService);
                }

                return survivabilityResponse;
            }
        }

        private JObject Serialise()
        {
            JObject requestRoot = new JObject();

            JObject inputs = new JObject();
            requestRoot.Add("Inputs", inputs);

            JObject passengerInput = new JObject();
            inputs.Add("input1", passengerInput);
            TitanicSurvivabilityRequest.Metadata.Serialise(this.Passenger, passengerInput);

            JObject globalParameters = new JObject();
            globalParameters.Add("Generate missing value indicator column", string.Empty);

            requestRoot.Add("GlobalParameters", globalParameters);

            return requestRoot;
        }
    }
}