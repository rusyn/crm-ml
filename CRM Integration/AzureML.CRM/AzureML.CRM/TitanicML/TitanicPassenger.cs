namespace AzureML.CRM.TitanicML
{
    internal class TitanicPassenger
    {
        public int PassengerId
        {
            get;
            set;
        }

        public string PassengerName
        {
            get;
            set;
        }

        public int PassengerClass
        {
            get;
            set;
        }

        public string Sex
        {
            get;
            set;
        }

        public int Age
        {
            get;
            set;
        }

        public int SiblingOrSpouseCount
        {
            get;
            set;
        }

        public int ParentsOrChildrenCount
        {
            get;
            set;
        }

        public string Ticket
        {
            get;
            set;
        }

        public double Fare
        {
            get;
            set;
        }

        public string Cabin
        {
            get;
            set;
        }

        public string Embarked
        {
            get;
            set;
        }
    }
}