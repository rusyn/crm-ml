﻿using System.Activities;
using AzureML.CRM.DependencyInjection;

namespace AzureML.CRM.Activities
{
    public class TitanicPopulateSurvivabilityWorkflowActivity : CodeActivity
    {
        protected override void Execute(CodeActivityContext context)
        {
            IDiContainer container = new DiContainer(context);

            TitanicPopulateSurvivabilityWorkflow workflow = container.Create<TitanicPopulateSurvivabilityWorkflow>();

            TitanicPopulateSurvivabilityArguments arguments = new TitanicPopulateSurvivabilityArguments();
            workflow.Execute(arguments);
        }
    }
}