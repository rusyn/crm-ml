﻿namespace AzureML.CRM.CrmExtensions
{
    internal interface IWorkflow<in T>
    {
        void Execute(T arguments);
    }
}