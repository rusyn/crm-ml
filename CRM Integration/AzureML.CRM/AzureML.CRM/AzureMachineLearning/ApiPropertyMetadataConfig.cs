namespace AzureML.CRM.AzureMachineLearning
{
    internal abstract class ApiPropertyMetadataConfig<T>
    {
        public abstract string PropertyName
        {
            get;
        }

        public abstract string GetValue(T instance);

        public abstract void SetValue(T instance, object rawValue);
    }
}