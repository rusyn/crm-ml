﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Linq.Expressions;
using System.Reflection;

namespace AzureML.CRM.AzureMachineLearning
{
    internal class PropertyBasedApiMetadataPropertyConfig<T> : ApiPropertyMetadataConfig<T>
    {
        private readonly CultureInfo culture;

        public PropertyBasedApiMetadataPropertyConfig(Expression<Func<T, object>> targetProperty)
        {
            this.culture = CultureInfo.GetCultureInfo("en-US");

            this.TargetProperty = targetProperty;
            this.TargetMember = PropertyBasedApiMetadataPropertyConfig<T>.ExtractMemberFromLambda(targetProperty);
        }

        private Expression<Func<T, object>> TargetProperty
        {
            get;
            set;
        }

        private MemberExpression TargetMember
        {
            get;
            set;
        }

        public bool Nullable
        {
            get;
            set;
        }

        public string ApiPropertyName
        {
            get;
            set;
        }

        public string StaticValue
        {
            get;
            set;
        }

        private static MemberExpression ExtractMemberFromLambda(Expression<Func<T, object>> property)
        {
            LambdaExpression lambda = property;
            MemberExpression memberExpression = null;

            switch (lambda.Body.NodeType)
            {
                case ExpressionType.Convert:
                    memberExpression = ((UnaryExpression) lambda.Body).Operand as MemberExpression;
                    break;

                case ExpressionType.MemberAccess:
                    memberExpression = lambda.Body as MemberExpression;
                    break;
            }

            if (memberExpression == null)
            {
                throw new ArgumentException("Lambda expression is not a supported expression", "property");
            }

            if (!(memberExpression.Member is PropertyInfo))
            {
                throw new ArgumentException("Lambda expression should express a property", "property");
            }

            return memberExpression;
        }

        public override string PropertyName
        {
            get
            {
                return this.ApiPropertyName ?? this.TargetMember.Member.Name;
            }
        }

        public override string GetValue(T instance)
        {
            Func<T, object> accessor = this.TargetProperty.Compile();
            object value = accessor.Invoke(instance);

            return value == null
                       ? null
                       : Convert.ToString(value);
        }

        public override void SetValue(T instance, object rawValue)
        {
            PropertyInfo property = (PropertyInfo) this.TargetMember.Member;

            TypeConverter typeConverter = TypeDescriptor.GetConverter(property.PropertyType);
            object coercedValue = typeConverter.ConvertFrom(null, this.culture, rawValue);

            property.SetValue(instance, coercedValue);
        }
    }
}